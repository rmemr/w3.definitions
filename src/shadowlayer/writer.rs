//
// definitions::shadowlayer::writer
//
// ----------------------------------------------------------------------------
use std::path::PathBuf;

use yaml::encoder::{IntoYaml as EncoderIntoYaml, ListEncoder, MapEncoder};
use yaml::{Yaml, YamlWriter};

use model::shadowlayer::{ShadowLayer, ShadowLayersDefinition, ShadowMeshEntity, ShadowMeshInfo};

use {IntoYaml, Result};

// ----------------------------------------------------------------------------
struct Writer;

impl YamlWriter for Writer {}
// ----------------------------------------------------------------------------
pub fn save_shadowlayer_definition(
    filename: PathBuf,
    definitions: &ShadowLayersDefinition,
) -> Result<()> {
    use chrono::prelude::Local;

    let creation_time = Local::now().format("%Y-%m-%d %H:%M:%S").to_string();

    info!("saving shadowlayer definition in [{}]", filename.display());

    let mut data = MapEncoder::from_key_value(".modid", definitions.id());
    for (world, layers) in definitions.hublayers() {
        data.add(".world", world);

        for (name, layer) in layers.layers() {
            trace!(">> serializing shadowlayer [{}]...", name);

            data.add(name, layer.into_yaml())
        }
    }

    let data = MapEncoder::from_key_value("layers.shadow", data);

    Writer::write_yaml(
        &filename,
        &format!(
            "#\n# autogenerated shadowmesh layers\n# \n# (saved: {})\n#\n",
            creation_time
        ),
        &data.intoyaml(),
    )
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for ShadowLayer {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut map = MapEncoder::new();
        // <name>:
        //   overwrite: true
        //   <entityname>:
        //      ...
        if *self.overwrite() {
            map.add(".overwrite", Yaml::Boolean(true));
        }

        for (name, entity) in self.entities() {
            map.add(name, entity.into_yaml());
        }
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ShadowMeshEntity {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut map = MapEncoder::new();
        // <entityname>:
        //    transform...
        //    meshes:
        //      -

        let mut list = ListEncoder::new();
        for mesh in self.meshes() {
            list.add(mesh.into_yaml());
        }
        map.add("transform", self.transform().into_yaml());
        map.add("meshes", list);
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ShadowMeshInfo {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut map = MapEncoder::new();

        map.add("transform", self.transform().into_yaml());
        map.add("src", self.srcmesh());
        map.add("shadowmesh", self.mesh());
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
