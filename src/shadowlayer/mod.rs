//
// definitions::shadowmesh
//
// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub use self::writer::save_shadowlayer_definition;
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
