//
// definitions::community community (yaml) defintions
//
// ----------------------------------------------------------------------------
mod spawntree;

mod reader;
// mod writer;

use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::community::spawntree::{
    AddItems, AddTag, DynamicWork, GuardArea, IdleAi, RainReaction, RiderAi, ScriptedInitializer,
    SetAppearance, SetAttitude, SetImmortality, SetLevel, WanderArea, WanderPath,
};
use model::community::{
    ActionPoint, Actor, ActorPhase, Community, QuestCommunities, SpawnSettings,
};
use model::ValidatableElement;
// ----------------------------------------------------------------------------
impl DatasetParser for QuestCommunities {
    type Dataset = Vec<(String, Community)>;
    // ------------------------------------------------------------------------
    fn parse_dataset(context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found quest community definitions...");

        let mut dataset = Vec::new();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (id, value) = kvparser.to_key_value()?;

            let id = id.to_lowercase();
            let mut community = Community::try_from_yaml(&id, &value)?;
            if let Some(context) = context {
                community.set_context(context);
                community
                    .validate("directory names in path")
                    .map_err(|e| format!("{}. found: {}", e, context))?;
            }
            dataset.push((id, community));
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for QuestCommunities {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if !data.is_empty() {
            debug!("> adding communities definition...");
            for (id, community) in data {
                self.add_community(id, community)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
