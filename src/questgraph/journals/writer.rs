//
// questgraph::journals::writer - quest block journal (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::encoder::{IntoYaml as EncoderIntoYaml, MapEncoder};
use yaml::Yaml;
use IntoYaml;

use model::questgraph::journals::{
    JournalElementReference, JournalEntry, JournalMappin, JournalObjective, JournalPhaseObjectives,
    JournalQuestOutcome,
};
use model::questgraph::QuestBlock;

use super::common;
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for JournalElementReference {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::JournalElementReference::*;

        match self {
            Character(character, entryid) => {
                Yaml::String(format!("characters/{}/{}", character, entryid))
            }
            Creature(creature, entryid) => {
                Yaml::String(format!("creatures/{}/{}", creature, entryid))
            }

            Quest(quest, entryid) => Yaml::String(format!("quests/{}/{}", quest, entryid)),

            Objective(quest, phase, objectiveid) => {
                Yaml::String(format!("{}/{}/{}", quest, phase, objectiveid))
            }
            Mappin(quest, phase, objectiveid, mappinid) => {
                Yaml::String(format!("{}/{}/{}/{}", quest, phase, objectiveid, mappinid))
            }
            PhaseObjectives(quest, phaseid) => Yaml::String(format!("{}/{}", quest, phaseid)),
            QuestOutcome(quest) => Yaml::String(quest.to_string()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for JournalEntry {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing journal-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // journal.ingrid:
        //   entry: "characters/ingrid/main"
        //   activate_root: true                # default is false
        //   notify: false                      # default is true
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        map.add("entry", self.reference().map(IntoYaml::into_yaml));
        map.add("notify", *self.notify());
        if *self.include_root() {
            map.add("activate_root", true);
        }

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for JournalObjective {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing objective-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // objective.start_new_quest:
        //   objective: examplequEStname/PHaseA/identIfy
        //   notify: true
        //   track: false
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        map.add("objective", self.reference().map(IntoYaml::into_yaml));
        map.add("notify", *self.notify());
        map.add("track", *self.track());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for JournalMappin {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing mappin-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // objective.start_new_quest:
        //   mappin: "examplequestname/phaseA/search/area1"
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        map.add("mappin", self.reference().map(IntoYaml::into_yaml));

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for JournalPhaseObjectives {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing phaseobjectives-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // phaseobjectives.start_new_quest:
        //   phase: examplequEStname/PHaseA
        //   notify: true
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        map.add("phase", self.reference().map(IntoYaml::into_yaml));
        map.add("notify", *self.notify());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for JournalQuestOutcome {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing questoutcome-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        let mut map = MapEncoder::new();
        // questoutcome.start_new_quest:
        //   quest: examplequEStname
        //   notify: true
        //   next: [...]
        common::serialize_editordata(self, &mut map);

        map.add("quest", self.reference().map(IntoYaml::into_yaml));
        map.add("notify", *self.notify());

        common::serialize_links(self, &mut map);

        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
