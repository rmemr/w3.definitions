//
// definitions::questgraph
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[macro_use]
mod common;
mod primitives;

mod conditions;
mod journals;
mod misc;
mod scripting;
mod segments;

use yaml::parser::Parser;
use {DatasetIntegrator, DatasetParser, Result, TryFromYaml};

use model::questgraph::segments::{QuestRootSegment, QuestSegment};
use model::questgraph::QuestStructure;
use model::ValidatableElement;

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
#[derive(Default)]
pub(super) struct QuestStructureDataset {
    root: Option<QuestRootSegment>,
    segments: Vec<(String, QuestSegment)>,
}
// ----------------------------------------------------------------------------
impl DatasetParser for QuestStructure {
    type Dataset = QuestStructureDataset;
    // ------------------------------------------------------------------------
    fn parse_dataset(context: Option<&str>, data: &Parser) -> Result<Self::Dataset> {
        info!("> found quest structure definition...");

        let mut dataset = QuestStructureDataset::default();
        let definition = data.as_map()?;

        for kvparser in definition.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "quest" => dataset.root = Some(parse_root_segment(&value)?),
                "segments" => dataset.segments = parse_subsegments(context, &value)?,

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        Ok(dataset)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DatasetIntegrator for QuestStructure {
    // ------------------------------------------------------------------------
    fn add_dataset(&mut self, data: Self::Dataset) -> Result<()> {
        if let Some(rootsegment) = data.root {
            self.set_root(rootsegment)
                .map_err(|err| yaml_errmsg!(err))?;
        }
        if !data.segments.is_empty() {
            debug!("> adding subsegment definitions...");
            for (segmentid, segment) in data.segments {
                self.add_segment(segmentid, segment)?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn parse_root_segment(data: &Parser) -> Result<QuestRootSegment> {
    debug!("> found quest structure root definition...");

    let mut quest = None;
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (key, value) = kvparser.to_key_value()?;

        match key.to_lowercase().as_str() {
            "blocks" => quest = Some(QuestRootSegment::try_from_yaml((), &value)?),
            _ => return yaml_err_unknown_key!(data.id(), key),
        }
    }
    quest.ok_or_else(|| yaml_errmsg!(format!("element [{}/{}] not found", data.id(), "blocks")))
}
// ----------------------------------------------------------------------------
fn parse_subsegments(context: Option<&str>, data: &Parser) -> Result<Vec<(String, QuestSegment)>> {
    debug!("> found quest structure subsegment definition...");

    let mut segments = Vec::new();
    let definition = data.as_map()?;

    for kvparser in definition.iter() {
        let (segmentid, value) = kvparser.to_key_value()?;

        let segmentid = segmentid.to_lowercase();
        let mut segment = QuestSegment::try_from_yaml(&segmentid, &value)?;
        if let Some(context) = context {
            segment.set_context(context);
            segment.set_external(true);
            segment
                .validate("directory names in path")
                .map_err(|e| format!("{}. found: {}", e, context))?;
        }

        segments.push((segmentid, segment));
    }
    Ok(segments)
}
// ----------------------------------------------------------------------------
