//
// questgraph::misc - misc quest block (yaml) definitions
//
// ----------------------------------------------------------------------------
// make community block parsing type safe depending on context
pub(super) struct Spawn;
pub(super) struct Despawn;
// ----------------------------------------------------------------------------
// make timemanagement block parsing type safe depending on context
pub(super) struct PauseTime;
pub(super) struct UnpauseTime;
pub(super) struct ShiftTime;
pub(super) struct SetTime;
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
use super::common;
// ----------------------------------------------------------------------------
