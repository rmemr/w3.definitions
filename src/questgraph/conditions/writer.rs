//
// questgraph::conditions::writer - quest block conditions (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::encoder::{IntoYaml as EncoderIntoYaml, ListEncoder, MapEncoder};
use yaml::Yaml;
use IntoYaml;

use model::questgraph::QuestBlock;

use model::questgraph::conditions::{Condition, WaitUntil};
use model::questgraph::conditions::{
    EnteredLeftAreaCondition, InsideOutsideAreaCondition, InteractionCondition, InteractionType,
    LoadingScreenCondition, LogicCondition, TimeCondition,
};
use model::shared::LogicOperation;

use super::common;
// ----------------------------------------------------------------------------
// TryIntoYaml impl
// ----------------------------------------------------------------------------
impl IntoYaml for WaitUntil {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing waituntil-block [{}]...",
            self.uid().blockid().name().as_str()
        );

        // waituntil.fact:
        //   factdb: [ somefactid, "=", 1 ]
        //   next:                          # must be next[.Out] for one condition
        //     - script.do_something

        // # multiple conditions possible, "name" attribute of condition maps to out socket (1:1 relation)
        // waituntil.something:
        //   conditions:
        //     cond1:                      # name which has to map outsocketid (here: -> next.cond1)
        //       factdb: [ somefactid_cond1, "<", 1 ]
        //     cond2:
        //       factdb: [ somefactid_cond2, ">", 1 ]
        //   next.cond1: script.do_something
        //   next.cond2: scene.somename2

        let conditions = self.conditions();
        let mut map = if conditions.len() == 1 {
            let condition = &conditions[0];

            if condition.name() == "Out" {
                let mut map = condition.into_yaml();

                common::serialize_editordata(self, &mut map);
                map
            } else {
                let mut map = MapEncoder::new();
                common::serialize_editordata(self, &mut map);

                map.add(condition.name(), condition.into_yaml());
                map
            }
        } else {
            let mut map = MapEncoder::new();
            common::serialize_editordata(self, &mut map);

            let mut cond_map = MapEncoder::new();
            for condition in conditions {
                cond_map.add(condition.name(), condition.into_yaml());
            }
            map.add("conditions", cond_map);
            map
        };

        common::serialize_links(self, &mut map);
        map.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for Condition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::Condition::*;

        match self {
            FactsDb(ref cond) => MapEncoder::from_key_value("factdb", cond.into_yaml()),
            Logic(ref cond) => cond.into_yaml(),
            Time(ref cond) => cond.into_yaml(),
            EnteredLeftArea(ref cond) => cond.into_yaml(),
            InsideOutsideArea(ref cond) => cond.into_yaml(),
            Interaction(ref cond) => cond.into_yaml(),
            LoadingScreen(ref cond) => cond.into_yaml(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for LogicCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        trace!(
            ">> serializing logic-condition-operator [{}]...",
            self.operation()
        );
        use self::LogicOperation::*;

        let mut conditions = ListEncoder::new();

        for c in self.conditions() {
            // subconditions always map to "Out"
            conditions.add(c.into_yaml());
        }

        let opname = match self.operation() {
            AND => "all",
            OR => "any",
            XOR => "exactlyone",
            NAND => "notall",
            NOR => "none",
            NXOR => "nxor",
        };

        MapEncoder::from_key_value(opname, conditions)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for EnteredLeftAreaCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        // entered:
        //   who: PLAYER            # optional, default player, case sensitive
        //   area: area2
        //   world: prologue
        //
        // shortest version:
        // entered: [prologue/area2, some_actor_tag]
        let mut data = ListEncoder::new_inlined();
        data.add(self.area().into_yaml());
        data.add(
            self.actor()
                .map_or_else(|| "PLAYER", |a| a.as_str())
                .intoyaml(),
        );

        let name = if *self.has_entered() {
            "entered"
        } else {
            "left"
        };
        MapEncoder::from_key_value(name, data)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for InsideOutsideAreaCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        // inside:
        //   who: PLAYER            # optional, default player, case sensitive
        //   area: area2
        //   world: prologue
        //
        // shortest version:
        // entered: [prologue/area2, some_actor_tag]
        let mut data = ListEncoder::new_inlined();
        data.add(self.area().into_yaml());
        data.add(
            self.actor()
                .map_or_else(|| "PLAYER", |a| a.as_str())
                .intoyaml(),
        );

        let name = if *self.is_inside() {
            "inside"
        } else {
            "outside"
        };
        MapEncoder::from_key_value(name, data)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for TimeCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::TimeCondition::*;
        // waituntil.beforeafternoon:
        //   before: "17:00"
        //
        // waituntil.morning:
        //   after: "05:00"
        //
        // waituntil.noon:
        //   time: "12:00"
        //
        // waituntil.delayed:
        //   elapsed: "01:00:10"
        //
        // checkif.morning:
        //   time: ["05:00", "10:00"]

        match self {
            After(time) => MapEncoder::from_key_value("after", time.into_yaml()),
            Before(time) => MapEncoder::from_key_value("before", time.into_yaml()),
            Time(time) => MapEncoder::from_key_value("time", time.into_yaml()),
            TimePeriod(from, until) => {
                let mut list = ListEncoder::new_inlined();
                list.add(from.into_yaml());
                list.add(until.into_yaml());

                MapEncoder::from_key_value("time", list)
            },
            Elapsed(time) => MapEncoder::from_key_value("elapsed", time.into_yaml()),
        }
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for InteractionCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        use self::InteractionType::*;
        // waituntil.beforeafternoon:
        //   interaction: [interactionname, prologue/something]      # interactionname unchecked, referenced interactive entity checked
        //   interaction: [interactionname, someone]                 # interactionname unchecked, referenced template checked
        //   interaction: [interactionname, ~componentownertag]      # completely unchecked
        //   examined: prologue/someclue1        # shortcut for interaction: [examine, prologue/someclue1]
        //   talked: someone                     # shortcut for interaction: [talk, someone]
        //   used: prologue/teleport             # shortcut for interaction: [use, prologue/teleport]
        //   looted: prologue/someloot           # shortcut for interaction: [loot, prologue/someloot]

        let entity = self.entity().into_yaml();

        match self.interaction() {
            Examine => MapEncoder::from_key_value("examined", entity),
            Talk => MapEncoder::from_key_value("talked", entity),
            Use => MapEncoder::from_key_value("used", entity),
            Loot => MapEncoder::from_key_value("looted", entity),
            Custom(name) => {
                let mut custom = ListEncoder::new_inlined();
                custom.add(name);
                custom.add(entity);
                MapEncoder::from_key_value("interaction", custom)
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for LoadingScreenCondition {
    type Output = MapEncoder;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        // loadscreen: "shown" | "hidden"

        let status = if *self.is_shown() {
            "shown"
        } else {
            "hidden"
        };
        MapEncoder::from_key_value("loadscreen", status)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
