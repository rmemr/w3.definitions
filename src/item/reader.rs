//
// item::reader
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use yaml::parser::Parser;
use {Result, TryFromYaml, ValidatableElement};

use super::{ItemDefinition, ItemReference, LootDefinition, RewardDefinition};
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for ItemDefinition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found item [{}] definition...", name);

        let mut item = ItemDefinition::new(name);
        // misc:
        //   item1:
        //     name: "Item 1"
        //     description: "Awesome Item 1"
        //  or:
        // notices:
        //   notice1:
        //     name: "Notice 1"
        //     description: "Some contract notice"
        //     text: "This is the text"

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "name" => item.set_caption(value.as_str()?),
                "description" => item.set_description(value.as_str()?),
                "text" => item.set_text(value.as_str()?),

                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }

        item.validate(data.id()).and(Ok(item))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for LootDefinition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found loot [{}] definition...", name);

        let mut loot = LootDefinition::new(name);
        // lootchest:
        //   - notice/item2
        //   - ~mq0002_box_loot

        for item in data.as_list()?.iter() {
            loot.add_item(ItemReference::try_from_yaml((), &item)?);
        }
        loot.validate(data.id()).and(Ok(loot))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for RewardDefinition {
    type Id = &'a str;
    // ------------------------------------------------------------------------
    fn try_from_yaml(name: &str, data: &Parser) -> Result<Self> {
        trace!(">> found reward [{}] definition...", name);

        let mut reward = RewardDefinition::new(name);
        // lootchest:
        //   - notice/item2
        //   - ~mq0002_box_loot

        for item in data.as_list()?.iter() {
            reward.add_item(ItemReference::try_from_yaml((), &item)?);
        }
        reward.validate(data.id()).and(Ok(reward))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
