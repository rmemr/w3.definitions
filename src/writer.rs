//
// definitions::writer
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub trait DatasetProvider {
    // ------------------------------------------------------------------------
    fn as_datasets(&self, outpath: &Path) -> Result<Vec<DataSet>, String>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub enum DataSet {
    // version, name, data
    Production(u32, String, Yaml),
    StructureRoot(u32, String, Yaml),
    // version, context, name, data
    StructureSegment(u32, Option<String>, String, Yaml),
    // name, data
    RawData(String, Vec<u8>),
}
// ----------------------------------------------------------------------------
pub struct DatasetWriter<T> {
    outputdir: PathBuf,
    _phantom: PhantomData<T>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::marker::PhantomData;
use std::path::{Path, PathBuf};

use super::yaml::{Yaml, YamlWriter};
// ----------------------------------------------------------------------------
impl<T> YamlWriter for DatasetWriter<T> {}
// ----------------------------------------------------------------------------
impl<T> DatasetWriter<T>
where
    T: DatasetProvider,
{
    // ------------------------------------------------------------------------
    pub fn new(outputdir: PathBuf) -> DatasetWriter<T> {
        DatasetWriter {
            outputdir,
            _phantom: PhantomData,
        }
    }
    // ------------------------------------------------------------------------
    fn write_rawdata(outputfile: &Path, data: &[u8]) -> Result<(), String> {
        use std::fs::File;
        use std::io::BufWriter;
        use std::io::Write;

        debug!("> creating {}...", outputfile.display());

        let mut buf = File::create(outputfile).map(BufWriter::new).map_err(|e| {
            format!(
                "raw data writer: couldn't create {}: {}",
                outputfile.display(),
                e
            )
        })?;

        buf.write(data)
            .map(|_| ())
            .map_err(|e| format!("raw data writer: write error: {}", e))
    }
    // ------------------------------------------------------------------------
    pub fn storedata(&self, data: &T) -> Result<(), String> {
        use chrono::prelude::Local;
        use std::fs;

        let creation_time = Local::now().format("%Y-%m-%d %H:%M:%S").to_string();

        if self.outputdir.is_dir() {
            let mut filename = self.outputdir.clone();
            // placeholder for changing filename in loop
            filename.push("-");

            for dataset in data.as_datasets(&self.outputdir)? {
                let filename = self.outputdir.join(dataset.filename());

                if let Some(parent_dir) = filename.parent() {
                    fs::create_dir_all(parent_dir).map_err(|why| {
                        format!(
                            "could not create directory {}: {}",
                            parent_dir.display(),
                            why
                        )
                    })?;
                }

                match dataset {
                    DataSet::Production(ref ver, _, ref data)
                    | DataSet::StructureRoot(ref ver, _, ref data)
                    | DataSet::StructureSegment(ref ver, _, _, ref data) => Self::write_yaml(
                        &filename,
                        &format!(
                            "#\n# {}\n# \n# (version: {}, saved: {})\n#\n",
                            dataset.description(),
                            ver,
                            creation_time
                        ),
                        data,
                    ),
                    DataSet::RawData(_, ref data) => Self::write_rawdata(&filename, data),
                }?;
            }
            Ok(())
        } else {
            Err(format!(
                "definition writer: [{}] is not a valid directory",
                self.outputdir.display()
            ))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DataSet {
    // ------------------------------------------------------------------------
    fn filename(&self) -> String {
        use self::DataSet::*;

        match self {
            Production(_, name, _) => format!("prod.quest-{}.yml", name),
            StructureRoot(_, _name, _) => String::from("structure.root.yml"),
            StructureSegment(_, context, name, _) => match context {
                Some(context) => format!("{}/structure.seg-{}.yml", context, name),
                None => format!("structure.seg-{}.yml", name),
            },
            RawData(name, _) => name.to_string(),
        }
    }
    // ------------------------------------------------------------------------
    fn description(&self) -> String {
        use self::DataSet::*;

        match self {
            Production(_, _, _) => String::from("production settings"),
            StructureRoot(_, _, _) => String::from("quest structure root segment"),
            StructureSegment(_, context, name, _) => match context {
                Some(context) => format!("quest structure subsegment: {}/{}", context, name),
                None => format!("quest structure subsegment: {}", name),
            },
            RawData(_, _) => String::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
