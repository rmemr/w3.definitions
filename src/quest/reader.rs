//
// definitions::quest::reader
//
// ----------------------------------------------------------------------------
use std::path::Path;

use yaml::parser::MapParser;
use yaml::Yaml;

use reader::{DatasetContainer, StatsOutput};

use model::community::QuestCommunities;
use model::item::{ItemDefinition, LootDefinition, RewardDefinition};
use model::journal::QuestJournals;
use model::layer::QuestLayers;
use model::navdata::NavData;
use model::questgraph::QuestStructure;
use model::resources::QuestResources;
use model::settings::QuestSettings;
use model::QuestDefinition;

use Result;
// ----------------------------------------------------------------------------
type RawData = (String, Vec<u8>);
// ----------------------------------------------------------------------------
fn extract_nonquest_definition(
    basedir: &Path,
    filepath: &Path,
    data: &MapParser,
) -> Result<Vec<RawData>> {
    for (def_type, id) in &[
        ("environment", "environment"),
        ("scene", "dialogscript"),
        ("scene", "storyboard"),
        ("hairworks", "furparameter"),
        // Note: hub definitions require dedicated folders (e.g. because
        // automatic definition backups on every save would require backup of
        // image maps => slow and backup folder size would grow very fast)
    ] {
        if data.get_value(id).is_some() {
            info!("> found {} definition...", def_type);
            let mut rawdata = vec![read_rawdata(basedir, filepath)?];
            // some definitions are accompanied by additional files
            if *def_type == "hairworks" {
                // read apx file
                rawdata.push(read_rawdata(basedir, &filepath.with_extension("apx"))?);
            }
            return Ok(rawdata);
        }
    }
    Ok(Vec::default())
}
// ----------------------------------------------------------------------------
fn read_rawdata(basedir: &Path, filepath: &Path) -> Result<RawData> {
    use std::fs::File;
    use std::io::Read;
    // as long as not all definition types are fully supported by
    // editor all unsupported definition files must be stored as raw
    // so they can be saved unmodified on project save
    let mut rawdata = Vec::new();

    let fullpath = basedir.join(filepath);
    trace!(
        ">> loading non-editable definitions as additional rawdata {}...",
        fullpath.display()
    );
    File::open(&fullpath)
        .and_then(|mut file| file.read_to_end(&mut rawdata))
        .map_err(|e| {
            format!(
                "definition reader: error reading {}: {}",
                fullpath.display(),
                e
            )
        })?;

    Ok((filepath.to_string_lossy().to_string(), rawdata))
}
// ----------------------------------------------------------------------------
use crate::{DatasetIntegrator, DatasetParser};

#[derive(Default)]
pub struct QuestDataset {
    settings: Option<QuestSettings>,
    structure: <QuestStructure as DatasetParser>::Dataset,
    layers: <QuestLayers as DatasetParser>::Dataset,
    journals: <QuestJournals as DatasetParser>::Dataset,
    communities: <QuestCommunities as DatasetParser>::Dataset,
    items: <ItemDefinition as DatasetParser>::Dataset,
    loot: <LootDefinition as DatasetParser>::Dataset,
    rewards: <RewardDefinition as DatasetParser>::Dataset,
    navdata: <NavData as DatasetParser>::Dataset,
    resources: <QuestResources as DatasetParser>::Dataset,

    rawdata: Vec<RawData>,
}
// ----------------------------------------------------------------------------
impl DatasetContainer<()> for QuestDefinition {
    type Dataset = QuestDataset;
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    fn parse(basedir: &Path, filepath: &Path, data: &Yaml, _: &()) -> Result<Self::Dataset> {
        debug!("parsing quest definition content...");

        let mut dataset = QuestDataset::default();

        let data = MapParser::new("quest definition", data)?;
        let mut serializeable = 0;
        let mut not_serializeable = 0;

        let context = filepath
            .parent()
            .map(|dir| dir.to_string_lossy().to_lowercase())
            .filter(|dir| !dir.is_empty());
        let context = context.as_deref();

        let errmsg = |e| format!("file: {}. {}", filepath.display(), e);

        // non-quest definitions:
        // peek into definition to identify types that are known but *not*
        // quest-definition. read those as raw data to allow writing complete
        // definition folder content into different folder
        let rawdatasets = extract_nonquest_definition(basedir, filepath, &data)?;
        if !rawdatasets.is_empty() {
            for rawdata in rawdatasets {
                dataset.rawdata.push(rawdata);
            }
        } else {
            for kvparser in data.iter() {
                let (key, value) = kvparser.to_key_value()?;
                let key = key.to_lowercase();
                let key = key.as_str();

                let is_serializeable = match key {
                    "production" => {
                        dataset.settings =
                            Some(QuestSettings::parse_dataset(context, &value).map_err(errmsg)?);
                        true
                    }
                    "structure" => {
                        dataset.structure =
                            QuestStructure::parse_dataset(context, &value).map_err(errmsg)?;
                        true
                    }
                    "layers" => {
                        dataset.layers =
                            QuestLayers::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "journals" => {
                        dataset.journals =
                            QuestJournals::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "communities" | "spawnsets" => {
                        dataset.communities =
                            QuestCommunities::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "entities" | "templates" => false,
                    "items" => {
                        dataset.items =
                            ItemDefinition::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "loot" => {
                        dataset.loot =
                            LootDefinition::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "rewards" => {
                        dataset.rewards =
                            RewardDefinition::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "navdata" => {
                        dataset.navdata =
                            NavData::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    "resources" => {
                        dataset.resources =
                            QuestResources::parse_dataset(context, &value).map_err(errmsg)?;
                        false
                    }
                    _ => return yaml_err_unknown_key!("quest definition", key),
                };
                if is_serializeable {
                    serializeable += 1;
                } else {
                    not_serializeable += 1;
                }
            }
        }

        if serializeable > 0 && not_serializeable > 0 {
            Err(format!(
                "definition reader: files containing editable (production or structure) and \
            non-editable definitions are not supported. split manually into separate files and \
            try again. file: {}",
                filepath.display()
            ))
        } else {
            if not_serializeable > 0 {
                dataset.rawdata.push(read_rawdata(basedir, filepath)?);
            }
            Ok(dataset)
        }
    }
    // ------------------------------------------------------------------------
    fn add(&mut self, mut data: Self::Dataset) -> Result<()> {
        // -- production settings
        if let Some(settings) = data.settings.take() {
            if self.settings().is_some() {
                return yaml_err!("only one production definition allowed.");
            } else {
                self.set_settings(settings);
            }
        }

        // -- quest structure
        self.structure_mut().add_dataset(data.structure)?;

        // -- quest layers
        self.layers_mut().add_dataset(data.layers)?;

        // -- quest journals
        self.journals_mut().add_dataset(data.journals)?;

        // -- quest communities
        self.communities_mut().add_dataset(data.communities)?;

        // -- quest items
        let items = self.items_mut();
        // data.items.iter().drain(..)
        data.items
            .drain(..)
            .try_for_each(|(id, item)| items.add_item(id, item))?;

        data.loot
            .drain(..)
            .try_for_each(|(id, loot)| items.add_lootdef(id, loot))?;

        data.rewards
            .drain(..)
            .try_for_each(|(id, reward)| items.add_reward(id, reward))?;

        // -- hub navdata
        self.navdata_mut().add_dataset(data.navdata)?;

        // -- quest generic resources
        self.resources_mut().add_dataset(data.resources)?;

        for rawdata in data.rawdata {
            self.add_rawdata(rawdata);
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl StatsOutput for QuestDefinition {
    // ------------------------------------------------------------------------
    fn log_stats(&self) {}
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
