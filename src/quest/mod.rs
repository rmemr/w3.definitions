//
// definitions::quest
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub fn load_repo(repodir: &Path) -> Result<Repository> {
    let reporeader = Reader::<Repository, ()>::new(repodir, "*.repo.yml")?;
    info!("scanning for repository files [{}]", reporeader.wildcard());

    let repo = reporeader.readdata(Repository::default(), &())?;
    if repo.is_empty() {
        Err("Invalid repository: no definitions found!".into())
    } else {
        repo.log_stats();
        Ok(repo)
    }
}
// ----------------------------------------------------------------------------
#[allow(clippy::bind_instead_of_map)]
pub fn load_def(defdir: &Path, _repo: &Repository) -> Result<QuestDefinition> {
    let reader = Reader::<QuestDefinition, ()>::new(defdir, "*.yml")?;
    info!(
        "scanning for quest definition files [{}]",
        reader.wildcard()
    );

    reader
        .readdata(QuestDefinition::default(), &())
        .and_then(|repo| {
            repo.log_stats();
            Ok(repo)
        })
}
// ----------------------------------------------------------------------------
pub fn save_def(defdir: PathBuf, definition: &QuestDefinition) -> Result<()> {
    info!("saving quest definition in [{}]", defdir.display());
    let writer = Writer::<QuestDefinition>::new(defdir);

    writer.storedata(definition)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
use std::path::{Path, PathBuf};

use reader::{DatasetReader as Reader, StatsOutput};
use writer::DatasetWriter as Writer;

use model::QuestDefinition;

use repository::Repository;
use Result;
// ----------------------------------------------------------------------------
